const accordionItem = () => {
  const items = document.querySelectorAll('.accordion-item');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      const arrow = item.querySelector('.accordion-item__arrow');
      const arrowAnimateOpen = arrow.querySelector('#open');
      const arrowAnimateClose = arrow.querySelector('#close');
      if (event.target.closest('.accordion-item__header')) item.classList.toggle('accordion-item--active');
      if (item.classList.contains('accordion-item--active')) arrowAnimateOpen.beginElement();
      else arrowAnimateClose.beginElement();
    });
  });
};

export default accordionItem;
