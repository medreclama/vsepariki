var homeCatalogSwitcher = document.getElementById('home-catalog__mobile-switch')
var homeCatalogTabActive = document.getElementsByClassName('home-catalog__tab_active')

var homeCatalogTabs = document.querySelectorAll('.home-catalog__tab')
var homeCatalogTabsBlock = document.querySelectorAll('.home-catalog__tabs')
var productTabs = document.querySelectorAll('.product-tabs__tab')

if (homeCatalogSwitcher && homeCatalogTabActive.length) {

  homeCatalogSwitcher.innerText = homeCatalogTabActive[0].innerText
  function homeCatalogTabSwitch() {
    var homeCatalogTabsContents = document.querySelectorAll('.home-catalog__tab-content')
    var currentTabName = this.innerText;
    for (var j = 0; j < homeCatalogTabsContents.length; j++) {
      homeCatalogTabsContents[j].classList.remove('home-catalog__tab-content_active');
      homeCatalogTabs[j].classList.remove('home-catalog__tab_active')
      this.classList.add('home-catalog__tab_active')
      this.parentElement.classList.remove('home-catalog__tabs_m-active')
      homeCatalogSwitcher.innerText = currentTabName
      if ( this.dataset.homeCatalogTab === homeCatalogTabsContents[j].dataset.homeCatalogTab ) {
        homeCatalogTabsContents[j].classList.add('home-catalog__tab-content_active')
      }
    }
  }
  homeCatalogSwitcher.addEventListener('click',homeCatalogSwitch)
  function homeCatalogSwitch() {
    if (!homeCatalogTabsBlock[0].classList.contains('home-catalog__tabs_m-active')){
      homeCatalogTabsBlock[0].classList.add('home-catalog__tabs_m-active')
      homeCatalogSwitcher.classList.add('home-catalog__mobile-switch_active')
    } else {
      homeCatalogTabsBlock[0].classList.remove('home-catalog__tabs_m-active')
      homeCatalogSwitcher.classList.remove('home-catalog__mobile-switch_active')
    }
  }
  for ( var i = 0; i < homeCatalogTabs.length; i++ ) {
    homeCatalogTabs[i].addEventListener ('click', homeCatalogTabSwitch)
  }
}
if (productTabs) {
  function productTabSwitch(){
    var productTabsContents = document.querySelectorAll('.product-tabs__tab-content')
    for ( var k = 0; k < productTabs.length; k++ ) {
      productTabs[k].classList.remove('product-tabs__tab_active')
    }
    this.classList.add('product-tabs__tab_active')
    for (var j = 0; j < productTabsContents.length; j++) {

      productTabsContents[j].classList.remove('product-tabs__tab-content_active');
      if ( this.dataset.productTab === productTabsContents[j].dataset.productTab ) {
        productTabsContents[j].classList.add('product-tabs__tab-content_active')
      }
    }
    for (var j = 0; j < productTabs.length; j++) {
      if ( this.dataset.productTab === productTabs[j].dataset.productTab && this !== productTabs[j] ) {
        productTabs[j].classList.add('product-tabs__tab_active')
      }
    }
  }
  for ( var i = 0; i < productTabs.length; i++ ) {
    productTabs[i].addEventListener ('click', productTabSwitch)
  }
}
var catalogProducts = document.getElementsByClassName('catalog-product');

// if (catalogProducts) {
//   $(window).on('load',function () {
//     for ( var i = 0; i < catalogProducts.length; i++ ) {
//       catalogProducts[i].parentElement.style.height = getComputedStyle(catalogProducts[i]).height;
//     }
//   })

//   $ ( window ) . resize (function () {
//     for ( var i = 0; i < catalogProducts.length; i++ ) {
//       catalogProducts[i].parentElement.style.height = getComputedStyle(catalogProducts[i]).height;
//     }
//   });
// }

$('.fotorama').fotorama({
  width:'100%',
  ratio: '800/390',
  loop: true,
  arrows: true
});

$('.link-interactive').click(function(event) {
  $(this).modal({
    showClose: false
  });
  return false;
});


var menuProductsItem = document.querySelectorAll ('.navigation-mobile__item');
for ( var i = 0 ; i < menuProductsItem.length ; i++ ) {

  if ( menuProductsItem[i].querySelector ('.nav-submenu')) {
    menuProductsItem[i].classList.add('navigation-mobile__item_parent')
    var parentSwitcher = document.createElement('div');
    parentSwitcher.classList.add('navigation-mobile__parent-switcher')
    menuProductsItem[i].insertBefore(parentSwitcher, menuProductsItem[i].children[1])
  }
}
var parentSwitchers = document.querySelectorAll ('.navigation-mobile__parent-switcher');
for ( var i = 0 ; i < parentSwitchers.length ; i++ ) {
  parentSwitchers[i].addEventListener('click', function() {
    if ( !this.nextElementSibling.classList.contains( 'nav-submenu_display' ) ) {
      this.nextElementSibling.classList.add( 'nav-submenu_display' )
    } else {
      this.nextElementSibling.classList.remove( 'nav-submenu_display' )
    }
  })
}


//установка последнего дня текущего месяца для акции

var date = new Date();
var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var months = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря']
var actionDate = document.getElementsByClassName('js-action-date');
for ( i = 0; i < actionDate.length; i++) {
  actionDate[i].innerText = lastDay.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
}

// Обратный отсчет времени до окончания периода действия скидки
$(function(){
  var today = new Date;
  today.setHours(0, 0, 0, 0);

  var curDate = today.getDate(); // сегодняшнее число месяца
  var curWeekDay = today.getDay(); // индекс сегодняшнего дня недели

  var daysToEndOfWeek = 7 - curWeekDay;
  var daysToNextWednesday = daysToEndOfWeek + 4; // Дней до след среды

  today.setDate(curDate + daysToNextWednesday);

  $('.item-product__discount-period #countdown').countdown({
    timestamp: today,
  });
});
// /Обратный отсчет времени до окончания периода действия скидки

//поведение мобильного фильтра

var mobileFilterSelects = document.querySelectorAll('.mobile-filter-item__select')
function mobileFilterActive (event) {
  var currentOptions = [];
  var activeLabel = this.parentNode.querySelector('.mobile-filter-item__label_active');
  var inactiveLabel = this.parentNode.querySelector('.mobile-filter-item__label_inactive');
  for ( var j = 0 ; j < this.options.length ; j++ ) {
    var option = this.options[j];
    if (option.selected) {
      currentOptions.push(option.innerText);
    }
  }
  if (currentOptions.length > 0) {
    activeLabel.innerText = currentOptions.join(', ');
    inactiveLabel.style.display = 'none';
  } else {
    activeLabel.innerText = ''
    inactiveLabel.style.display = 'block';
  }
}
for ( var i = 0 ; i < mobileFilterSelects.length ; i++ ) {
  mobileFilterSelects[i].addEventListener ( 'focusout' , mobileFilterActive )
  mobileFilterSelects[i].addEventListener ( 'click' , mobileFilterActive )
  mobileFilterSelects[i].addEventListener ( 'change' , mobileFilterActive )
  
  
}


document.addEventListener("DOMContentLoaded", function () {
  for ( var i = 0 ; i < mobileFilterSelects.length ; i++ ) {
      var currentOptions = [];
  var activeLabel = mobileFilterSelects[i].parentNode.querySelector('.mobile-filter-item__label_active');
  var inactiveLabel = mobileFilterSelects[i].parentNode.querySelector('.mobile-filter-item__label_inactive');
  for ( var j = 0 ; j < mobileFilterSelects[i].options.length ; j++ ) {
    var option = mobileFilterSelects[i].options[j];
    if (option.selected) {
      currentOptions.push(option.innerText);
    }
  }
  if (currentOptions.length > 0) {
    activeLabel.innerText = currentOptions.join(', ');
    inactiveLabel.style.display = 'none';
  } else {
    activeLabel.innerText = ''
    inactiveLabel.style.display = 'block';
  }
  }
})

var headerMobile = document.querySelector( '.header-mobile' );

headerMobile.addEventListener( 'click', ( e ) => {
  let hmm = headerMobile.querySelector( '.header-mobile__menu' );
  let hmp = headerMobile.querySelector( '.header-mobile__phone' );
  let hms = headerMobile.querySelector( '.header-mobile__search' );
  let hmss = headerMobile.querySelector( '.header-mobile__search-switch' );
  let searchInput = hms.querySelector('.header-search__input');
  if ( e.target == hmss ) {
    hmm.classList.toggle( 'header-mobile__menu--inactive' );
    hmp.classList.toggle( 'header-mobile__phone--inactive' );
    hms.classList.toggle( 'header-mobile__search--active' );
    hmss.classList.toggle( 'header-mobile__search-switch--active' );
    if ( hms.classList.contains( 'header-mobile__search--active' ) ) {
      searchInput.focus()
    } else {
      searchInput.focus()
    }
  }
} )

const instaWidget = (settings = {element: ''}) => {

  class InstaWidget {
    constructor(settings = {
      elem: '',
      userId: '',
      title: '',
    }) {
      this.elem = settings.elem;

      this.userId = settings.userId || '';

      if (this.elem.dataset.title) this.title = this.elem.dataset.title;
      else this.title = settings.title;

      const url = settings.url || {};

      this.url = {
        getName: url.getName || '/instagram-widget/index.php?get_user_name=Y',
        getMedia: url.getMedia || '/instagram-widget/index.php',
        refreshToken: url.refreshToken || '/instagram-widget/index.php?refresh=Y',
      };
    }

    async userName() {
      const response = await fetch(this.url.getName);
      const user = await response.text();
      return user;
    }

    init() {
      new Promise((res) => {
        res(this.userName());
      })
        .then((res) => {
          this.userId = this.userId ? this.userId : res;
          this.title = this.title ? this.title : res;
        })
        .then(() => {
          if (this.elem.children.length > 0 || !this.url) {
            return;
          }
          const list = document.createElement('div');
          const title = document.createElement('div');
          const titleLink = document.createElement('a');
          const filter = this.elem.dataset.tagFilter ? (
            this.elem.dataset.tagFilter
              .split(',')
              .map((item) => `#${item.trim()}`)
          ) : [''];
          const className = 'instagram-widget';
          this.elem.classList.add(className);

          list.classList.add(`${className}__list`);
          title.classList.add(`${className}__title`);
          titleLink.setAttribute('href', `https://www.instagram.com/${this.userId}/`);
          titleLink.setAttribute('target', '_blank');
          titleLink.innerText = `${this.title}`;
          title.appendChild(titleLink);
          this.elem.appendChild(title);

          fetch(this.url.getMedia)
            .then((response) => response.json())
            .then((response) => response.data.filter((item) => filter.map((filterItem) => item.caption.includes(filterItem)).filter((i) => i === true).length > 0)) // eslint-disable-line max-len
            .then((result) => {
              const data = result.slice(0, 8);
              data.forEach((item) => {
                const inwidgetItem = document.createElement('div');
                const inwidgetItemLink = document.createElement('a');
                const inwidgetItemImage = document.createElement('img');
                inwidgetItem.className = `${className}__item`;
                inwidgetItemLink.href = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemLink.dataset.fancybox = 'inwidget';
                inwidgetItemImage.src = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemImage.alt = item.caption;
                inwidgetItemLink.appendChild(inwidgetItemImage);
                inwidgetItem.appendChild(inwidgetItemLink);
                list.appendChild(inwidgetItem);
              });
              this.elem.appendChild(list);
            })
            .catch(() => {
              this.elem.remove();
            });
        });
    }
  }
  const selector = settings.element || '.instagram-widget';
  const inwidget = Array.from(document.querySelectorAll(selector));

  inwidget.forEach((item) => {
    const widget = new InstaWidget({ 'elem': item });
    widget.init();
  })
}

instaWidget();

const nodeScriptIs = (node) => node.tagName === 'SCRIPT';

const nodeScriptClone = (node) => {
  const script = document.createElement('script');
  script.text = node.innerHTML;
  let i = 0;
  let attr;
  const attrs = node.attributes;
  while (i < attrs.length) {
    script.setAttribute((attr = attrs[i]).name, attr.value);
    i += 1;
  }
  return script;
};

const nodeScriptReplace = (node) => {
  if (nodeScriptIs(node) === true) {
    node.parentNode.replaceChild(nodeScriptClone(node), node);
  } else {
    let i = 0;
    const children = node.childNodes;
    while (i < children.length) {
      nodeScriptReplace(children[i]);
      i += 1;
    }
  }

  return node;
};

const counters = (code, where = 'body') => {
  const place = ['body', 'head'].includes(where) ? where : 'body';
  const block = document.createElement('div');
  block.innerHTML = code;
  const userEvents = () => {
    window.removeEventListener('scroll', userEvents);
    window.removeEventListener('mousemove', userEvents);
    document[place].append(...nodeScriptReplace(block).children);
  };
  window.addEventListener('scroll', userEvents);
  window.addEventListener('mousemove', userEvents);
};

const countersCodeHead = `
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W6B47S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W6B47S');</script>
<!-- End Google Tag Manager -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(39305555, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/39305555" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
		<script type="text/javascript">
		var __cs = __cs || [];
		__cs.push(["setCsAccount", "KggM6rPznstIFS43VCgGYviXrUs_vE0e"]);
		__cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
		</script>
		<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
`;
//
// const countersCodeBody = `
//
// `;

counters(countersCodeHead, 'head');
// counters(countersCodeBody);

const mrcSH = () => {
  const mrcSHTitles = Array.from(document.querySelectorAll('[data-mrc-sh-title]'));
  const mrcSHBodies = Array.from(document.querySelectorAll('[data-mrc-sh-body]'));
  mrcSHTitles.forEach((title) => {
    const itemObj = { name: title, bodies: [] };
    const itemName = title.dataset.mrcShTitle;
    const bodiesList = [];
    mrcSHBodies.forEach((body) => {
      const itemBody = body;
      const bodyName = itemBody.dataset.mrcShBody;
      if (itemName === bodyName) {
        bodiesList.push(itemBody);
        itemBody.style.display = 'none';
      }
    });
    if (bodiesList) {
      itemObj.bodies = bodiesList;
      itemObj.name.addEventListener('click', (e) => {
        e.preventDefault();
        bodiesList.forEach((item) => {
          const currentItem = item;
          currentItem.style.display = currentItem.style.display === 'none' ? '' : 'none';
        });
      });
    }
  });
};

mrcSH();

const countdowns = document.querySelectorAll('.countdown');
countdowns.forEach((cd) => {
  const date = cd.dataset.date.split(',');
  jQuery(cd).countdown({ timestamp: new Date(...date) });
});

// Сумма рейтинга для статей
const addStarGradient = (container, rating) => {
  container.forEach((star, i) => {
    if (i < Math.floor(rating)) star.style.setProperty('--gradient-stop', '100%');
    else if (i === Math.floor(rating)) star.style.setProperty('--gradient-stop', `${(rating - Math.floor(rating)) * 100}%`);
  });
};

const ratingInit = () => {
  const ratingSum = document.querySelector('.rating-sum');
  if (ratingSum) {
    const stars = Array.from(ratingSum.querySelectorAll('.rating-sum__star'));
    const ratingCount = ratingSum.querySelector('.rating-sum__count');
    ratingCount.innerHTML = ratingSum.dataset.rating;
    addStarGradient(stars, ratingSum.dataset.rating);
  }
};
ratingInit();

// Содержание для статей
const articleContent = () => {
  const article = document.querySelector('.news-list');
  const articleHeader = document.querySelector('.article-header');
  if (!article || !articleHeader) return;
  const titles = article.querySelectorAll('h2');
  const filteredTitles = Array.from(titles).filter(title => title.innerHTML !== 'Поделиться' && title.innerHTML !== 'Оценить статью');

  if (filteredTitles.length > 0) {
    const content = document.createElement('div');
    const contentBody = document.createElement('div');

    content.classList.add('article-content');
    contentBody.classList.add('article-content__body');

    content.innerHTML = '<h2 class="article-content__title">Содержание<span class="article-content__arrow"></span></h2>';
    content.append(contentBody);

    content.addEventListener('click', (event) => {
      if (event.target.closest('.article-content__title')) content.classList.toggle('article-content--active');
    });

    filteredTitles.forEach((title, index) => {
      if (title.innerHTML !== 'Поделиться' && title.innerHTML !== 'Оценить статью') {
        title.setAttribute('id', `title-${index}`);
        const link = document.createElement('a');
        link.classList.add('article-content__link');
        link.setAttribute('href', `#title-${index}`);
        link.innerHTML = title.innerHTML;
        contentBody.append(link);
      }
    });
    articleHeader.after(content);
  }
};

articleContent();

// Скрипт для госования
const ratingVote = () => {
  const ratingContainer = document.querySelector('.rating');
  if (!ratingContainer) return;
  const ratingVoteMessage = ratingContainer.querySelector('.rating__vote');
  const articleUrl = window.location.pathname;
  const ratedStarsKey = `ratedStars_${articleUrl}`;
  const ratedStars = localStorage.getItem(ratedStarsKey);
  const ratingInputs = document.querySelectorAll('.rating input[type="radio"]');
  let hasVoted = false;
  const message = document.createElement('p');
  message.innerHTML = 'Вы уже проголосовали';

  const disableRating = () => {
    ratingInputs.forEach((input) => {
      input.disabled = true;
    });
  };

  const showRating = (ratingStarMessage) => {
    ratingVoteMessage.innerHTML = ratingStarMessage;
  };

  const rateStar = (ratingStar) => {
    localStorage.setItem(ratedStarsKey, ratingStar);
    disableRating();
    hasVoted = true;
  };

  if (ratedStars) {
    document.getElementById(`star-${ratedStars}`).checked = true;
    showRating(ratedStars);
    disableRating();
    ratingContainer.addEventListener('click', () => {
      ratingContainer.after(message);
    });
  } else {
    ratingContainer.addEventListener('click', (evt) => {
      if (evt.target.tagName === 'INPUT') {
        showRating(evt.target.value);
        rateStar(evt.target.value);
      } else if (hasVoted) ratingContainer.after(message);
    });
  }
};

ratingVote();

const homeDiscountSlider = new Swiper('.home-discount-slider__slider .slider__slides', {
  loop: true,
  pagination: {
    el: '.slider__pagination',
    bulletClass: 'slider__pagination-item',
    bulletActiveClass: 'slider__pagination-item--active',
  },
  navigation: {
    nextEl: '.slider__arrow--next',
    prevEl: '.slider__arrow--prev',
    disabledClass: 'slider__arrow--disabled',
  },
});

const articlePreviewSlider = new Swiper('.article-preview .slider__slides', {
  spaceBetween: 24,
  breakpoints: {
    320: {
      slidesPerView: 1.25,
    },
    480: {
      slidesPerView: 2,
    },
    800: {
      slidesPerView: 3,
    },
    1000: {
      slidesPerView: 4,
    },
  },
  navigation: {
    nextEl: '.slider__arrow--next',
    prevEl: '.slider__arrow--prev',
    disabledClass: 'slider__arrow--disabled',
  },
});

const accordionItem = () => {
  const items = document.querySelectorAll('.accordion-item');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      const arrow = item.querySelector('.accordion-item__arrow');
      const arrowAnimateOpen = arrow.querySelector('#open');
      const arrowAnimateClose = arrow.querySelector('#close');
      if (event.target.closest('.accordion-item__header')) item.classList.toggle('accordion-item--active');
      if (item.classList.contains('accordion-item--active')) arrowAnimateOpen.beginElement();
      else arrowAnimateClose.beginElement();
    });
  });
};

accordionItem();

// Открытие модального окна "В корзину" в каталоге
$('.modal-link-add-to-cart').click(function() {
  $(this).modal({
    showClose: false
  });
});

// $('.modal-link-availability').click(function() {
// 	let product_id = $(this).data('item');
// 	$('.product_art').val(product_id);
// 	console.log(product_id);
//     $(this).modal({
//         showClose: false
//     });
//     return false;
// });

const video360Init = () => {
  const video360 = document.getElementById('video360');
  if (!video360) return;
  // eslint-disable-next-line no-undef
  const player = videojs('video360', {
    controls: true,
    autoplay: false,
    playsinline: true,
    preload: 'auto',
    loop: true,
    aspectRatio: '16:9',
  });

  player.on('ready', () => {
    player.removeClass('vjs-user-inactive');
    player.on('mousemove', () => {
      player.removeClass('vjs-user-inactive');
    });

    player.on('touchstart', () => {
      player.removeClass('vjs-user-inactive');
    });

    player.on('touchmove', () => {
      player.removeClass('vjs-user-inactive');
    });

    player.on('play', () => {
      player.removeClass('vjs-user-inactive');
    });
  });

  player.vr({
    projection: '360',
    debug: false,
  });
};

video360Init();
