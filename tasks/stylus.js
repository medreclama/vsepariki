import gulp from 'gulp';
import gulpIf from 'gulp-if';
import stylus from 'gulp-stylus';
import stylint from 'gulp-stylint';
import plumber from 'gulp-plumber';
import sourcemaps from 'gulp-sourcemaps';
import importIfExist from 'stylus-import-if-exist';
import autoprefixer from 'autoprefixer-stylus';
import gcmq from 'gulp-group-css-media-queries';
import csso from 'gulp-csso';
import { isDebug, templateDir } from '../gulpfile.babel';

gulp.task('stylus', (done) => {
  gulp.src('./src/styles/*.styl')
    .pipe(plumber())
    .pipe(gulpIf(isDebug, sourcemaps.init()))
    .pipe(stylus({
      use: [
        importIfExist(),
        autoprefixer(),
      ],
      'include css': true,
    }))
    .pipe(gulpIf(!isDebug, gcmq()))
    .pipe(gulpIf(!isDebug, csso()))
    .pipe(gulpIf(isDebug, sourcemaps.write()))
    .pipe(gulp.dest(`${templateDir}/styles`));
  done();
});

gulp.task('stylint', (done) => {
  gulp.src(['src/**/*.styl', '!src/styles/**'])
    .pipe(plumber())
    .pipe(stylint())
    .pipe(stylint.reporter())
    .pipe(stylint.reporter('fail', { failOnWarning: true })); // Линтер падает не только при ошибках, но и при warning'ах. Нужно для работы husky.
  done();
});
