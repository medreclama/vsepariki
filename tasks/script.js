import gulp from 'gulp';
import plumber from 'gulp-plumber';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import webpackConf from '../webpack.config';

import { distDir } from '../gulpfile.babel';

gulp.task('script', (done) => {
  gulp.src('./src/scripts/script.js')
    .pipe(plumber())
    .pipe(webpackStream(webpackConf, webpack))
    .pipe(gulp.dest(`${distDir}scripts/`));
  done();
});
